package camauth

import (
	"errors"
	"net/http"
	"net/url"
	"strings"
)


type CAM struct {
	GatewayUrl string
}

func (c *CAM) GetPassport(user, pass string) (string, error) {
	client := &http.Client{}
	resp, err := client.PostForm(c.GatewayUrl, url.Values{"CAMUsername": {user}, "CAMPassword": {pass}})
	if err != nil {
		return "", err
	}
	return extractCookie(resp.Header, "cam_passport")
}

func (c *CAM) VerifyPassport(pass string) error {
	client := &http.Client{}
	request, err := http.NewRequest("GET", c.GatewayUrl, nil)
	if err != nil {
		return err
	}
	request.AddCookie(&http.Cookie{Name: "cam_passport", Value: pass})
	resp, err := client.Do(request)
	if err != nil {
		return err
	}
	_, err = extractCookie(resp.Header, "userCapabilities")
	return err
}

func extractCookie(header map[string][]string, name string) (string, error) {
	for _, cookie := range header["Set-Cookie"] {
		if strings.HasPrefix(cookie, name+"=") {
			parts := strings.Split(cookie, ";")
			passport := strings.Split(parts[0], "=")[1]
			return passport, nil
		}
	}
	return "", errors.New("Cookie '" + name + "' not found")
}
