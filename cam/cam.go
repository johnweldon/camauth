package main

import (
	"bitbucket.org/johnweldon/camauth"
	"fmt"
	"log"
	"os"
)

func main() {
	user := os.Getenv("CAM_USER")
	pass := os.Getenv("CAM_PASS")
	url := os.Getenv("CAM_URL")
	cam := &camauth.CAM{GatewayUrl: url}

	passport, err := cam.GetPassport(user, pass)
	if err != nil {
		log.Fatal(err)
	}

	err = cam.VerifyPassport(passport)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Fprintf(os.Stdout, "PASSPORT: %s\n", passport)
}
